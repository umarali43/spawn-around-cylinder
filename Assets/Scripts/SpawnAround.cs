﻿using UnityEngine;

public class SpawnAround : MonoBehaviour
{
    private float radius = 0.5f;
    /// <summary>
    /// It defines the gap between the trees spawned at random angles. Lesser the gap, more the number of trees
    /// </summary>
    [SerializeField] private float gap = 1f;
    [SerializeField] private GameObject[] prefabs;
    // Having this as hurdles parent, makes the the hurdles scale independant of cylinder's scale 
    [SerializeField] private Transform hurdlesParent;
    //Defines the scale of the hurdles
    [SerializeField] private float hurdlesScale = 15f;

    void Start()
    {
        StartSpawning();
    }

    /// <summary>
    /// Starts the spawning hurdles with linear gaps around the cylinder independant of the cylinder's scale.
    /// </summary>
    public void StartSpawning()
    {
        Bounds bounds = GetComponent<MeshFilter>().mesh.bounds;
        gap /= transform.localScale.y;
        Vector3 center = new Vector3(transform.localPosition.x, transform.localPosition.y - bounds.size.y / 2, transform.localPosition.z);
        float targetZPos = transform.localPosition.y + bounds.size.y / 2;
        while (center.y < targetZPos)
        {
            SpawnPrefab(prefabs[Random.Range(0, prefabs.Length)], center, Random.Range(0, 360));
            center = new Vector3(center.x, center.y + gap, center.z);
        }
    }

    /// <summary>
    /// Spawns the prefab and set its rotation outward of the cylinder
    /// </summary>
    /// <param name="HurdlePrefab">Hurdle prefab.</param>
    /// <param name="center">Center.</param>
    /// <param name="_angle">Angle.</param>
    public void SpawnPrefab(GameObject HurdlePrefab, Vector3 center, float _angle)
    {
        GameObject hurdle = Instantiate(HurdlePrefab, transform);
        hurdle.transform.localPosition = GetPos(center, _angle);
        hurdle.transform.localRotation = Quaternion.FromToRotation(Vector3.up, hurdle.transform.localPosition - center);
        hurdle.transform.parent = hurdlesParent;
        hurdle.transform.localScale = new Vector3(hurdlesScale, hurdlesScale, hurdlesScale);
    }

    /// <summary>
    /// Returns the position of the item to be spawned at the edge of the circle according to the center point along with the provided angle
    /// </summary>
    /// <returns>The position.</returns>
    /// <param name="center">Center.</param>
    /// <param name="_angle">Angle.</param>
    Vector3 GetPos(Vector3 center, float _angle)
    {
        float PosZ = center.z + radius * Mathf.Cos(_angle * Mathf.Deg2Rad);
        float PosX = center.x + radius * Mathf.Sin(_angle * Mathf.Deg2Rad);
        float PosY = center.y;
        return new Vector3(PosX, PosY, PosZ);
    }
}
